/*
 * Yang Shuai  Copyright (c) 2022 https://yby6.com.
 * Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
 * Please keep the information of the original author of the code. Thank you
 *
 */

import Vue from 'vue'
import ElementUI from 'element-ui'

Vue.use(ElementUI)
