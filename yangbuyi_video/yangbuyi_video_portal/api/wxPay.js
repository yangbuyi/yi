/*
 * Yang Shuai  Copyright (c) 2022 https://yby6.com.
 * Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
 * Please keep the information of the original author of the code. Thank you
 *
 */

// axios 发送ajax请求
import request from '@/utils/request'

export default {

  //Native下单
  nativePay(contentId) {
    return request({
      url: '/service_pay/wx-pay/native/' + contentId,
      method: 'post'
    })
  },

  //Native下单(v2)
  nativePayV2(contentId) {
    return request({
      url: '/service_pay/wx-pay-v2/native/' + contentId,
      method: 'post'
    })
  },

  cancel(orderNo) {
    return request({
      url: '/service_pay/wx-pay/cancel/' + orderNo,
      method: 'post'
    })
  },

  refunds(orderNo, reason, refundNo) {
    return request({
      url: '/service_pay/wx-pay/refunds/' + orderNo + '/' + refundNo + "/" + reason,
      method: 'post'
    })
  }
}
