// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;
import top.yangbuyi.service_base.controller.core.vo.BaseEntity;

/**
    * 退款表
    */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_refund_info")
public class RefundInfo extends BaseEntity {
    /**
     * 款单id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 商户订单编号
     */
    @TableField(value = "order_no")
    private String orderNo;

    /**
     * 商户退款单编号
     */
    @TableField(value = "refund_no")
    private String refundNo;

    /**
     * 支付系统退款单号
     */
    @TableField(value = "refund_id")
    private String refundId;

    /**
     * 原订单金额(分)
     */
    @TableField(value = "total_fee")
    private Integer totalFee;

    /**
     * 退款金额(分)
     */
    @TableField(value = "refund")
    private Integer refund;

    /**
     * 退款原因
     */
    @TableField(value = "reason")
    private String reason;

    /**
     * 退款状态
     */
    @TableField(value = "refund_status")
    private String refundStatus;

    /**
     * 申请退款返回参数
     */
    @TableField(value = "content_return")
    private String contentReturn;

    /**
     * 退款结果通知参数
     */
    @TableField(value = "content_notify")
    private String contentNotify;


    public static final String COL_ID = "id";

    public static final String COL_ORDER_NO = "order_no";

    public static final String COL_REFUND_NO = "refund_no";

    public static final String COL_REFUND_ID = "refund_id";

    public static final String COL_TOTAL_FEE = "total_fee";

    public static final String COL_REFUND = "refund";

    public static final String COL_REASON = "reason";

    public static final String COL_REFUND_STATUS = "refund_status";

    public static final String COL_CONTENT_RETURN = "content_return";

    public static final String COL_CONTENT_NOTIFY = "content_notify";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}