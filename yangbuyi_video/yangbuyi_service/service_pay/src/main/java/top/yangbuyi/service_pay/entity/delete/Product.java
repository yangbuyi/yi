// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_pay.entity.delete;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.yangbuyi.service_base.controller.core.vo.BaseEntity;

/**
    * 商品表
    */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_product")
public class Product extends BaseEntity {
    /**
     * 商Bid
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 商品名称
     */
    @TableField(value = "title")
    private String title;

    /**
     * 价格(分)
     */
    @TableField(value = "price")
    private Integer price;


    public static final String COL_ID = "id";

    public static final String COL_TITLE = "title";

    public static final String COL_PRICE = "price";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}