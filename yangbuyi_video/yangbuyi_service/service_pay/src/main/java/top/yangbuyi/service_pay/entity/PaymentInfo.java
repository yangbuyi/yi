// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;
import top.yangbuyi.service_base.controller.core.vo.BaseEntity;

/**
    * 微信支付详情表
    */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName(value = "t_payment_info")
public class PaymentInfo extends BaseEntity {
    /**
     * 支付记录id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 商户订单编号
     */
    @TableField(value = "order_no")
    private String orderNo;

    /**
     * 支付系统交易编号
     */
    @TableField(value = "transaction_id")
    private String transactionId;

    /**
     * 支付类型
     */
    @TableField(value = "payment_type")
    private String paymentType;

    /**
     * 交易类型
     */
    @TableField(value = "trade_type")
    private String tradeType;

    /**
     * 交易状态
     */
    @TableField(value = "trade_state")
    private String tradeState;

    /**
     * 支付金额(分)
     */
    @TableField(value = "payer_total")
    private Integer payerTotal;

    /**
     * 通知参数
     */
    @TableField(value = "content")
    private String content;


    public static final String COL_ID = "id";

    public static final String COL_ORDER_NO = "order_no";

    public static final String COL_TRANSACTION_ID = "transaction_id";

    public static final String COL_PAYMENT_TYPE = "payment_type";

    public static final String COL_TRADE_TYPE = "trade_type";

    public static final String COL_TRADE_STATE = "trade_state";

    public static final String COL_PAYER_TOTAL = "payer_total";

    public static final String COL_CONTENT = "content";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}