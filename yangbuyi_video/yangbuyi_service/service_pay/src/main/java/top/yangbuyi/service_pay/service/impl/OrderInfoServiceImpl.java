// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.yangbuyi.common_utils.enums.wxpay.OrderStatus;
import top.yangbuyi.service_pay.entity.delete.OrderInfo;
import top.yangbuyi.service_pay.entity.delete.Product;
import top.yangbuyi.service_pay.mapper.OrderInfoMapper;
import top.yangbuyi.service_pay.service.OrderInfoService;
import top.yangbuyi.service_pay.service.ProductService;
import top.yangbuyi.service_pay.utils.OrderNoUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {

    private final ProductService tProductService;

    /**
     * 创建订单
     *
     * @param productId 商品
     */
    @Override
    @Transactional
    public OrderInfo createOrderByProductId(Long productId) {
        // 查找已存在但未支付的订单
        OrderInfo orderInfo = this.lambdaQuery().eq(OrderInfo::getProductId, productId).eq(OrderInfo::getOrderStatus, OrderStatus.NOTPAY.getType()).one();
        if (orderInfo != null) {
            return orderInfo;
        }
        // 根据商品ID 获取 商品信息
        Product product = tProductService.lambdaQuery().eq(Product::getId, productId).one();
        // 创建订单信息
        orderInfo = new OrderInfo();
        orderInfo.setTitle(product.getTitle());
        orderInfo.setOrderNo(OrderNoUtils.getOrderNo());
        orderInfo.setProductId(productId);
        orderInfo.setTotalFee(product.getPrice()); // 分
        orderInfo.setOrderStatus(OrderStatus.NOTPAY.getType());
        save(orderInfo);
        return orderInfo;
    }

    @Override
    public List<OrderInfo> getNoPayOrderByDuration(int minutes) {
        // 获取当前时间未来的minutes时间 早于五分钟之前
        Instant instant = Instant.now().minus(Duration.ofMinutes(minutes));

        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_status", OrderStatus.NOTPAY.getType());
        queryWrapper.le("create_time", instant);

        return baseMapper.selectList(queryWrapper);
    }
}

