// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.entity.delete;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;
import top.yangbuyi.service_base.controller.core.vo.BaseEntity;

/**
 * 订单详情表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_order_info")
public class OrderInfo extends BaseEntity {
    /**
     * 订单id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 订单标题
     */
    @TableField(value = "title")
    private String title;

    /**
     * 商户订单编号
     */
    @TableField(value = "order_no")
    private String orderNo;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 支付产品id
     */
    @TableField(value = "product_id")
    private Long productId;

    /**
     * 订单金额(分)
     */
    @TableField(value = "total_fee")
    private Integer totalFee;

    /**
     * 订单二维码连接
     */
    @TableField(value = "code_url")
    private String codeUrl;

    /**
     * 订单状态
     */
    @TableField(value = "order_status")
    private String orderStatus;


    public static final String COL_ID = "id";

    public static final String COL_TITLE = "title";

    public static final String COL_ORDER_NO = "order_no";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_TOTAL_FEE = "total_fee";

    public static final String COL_CODE_URL = "code_url";

    public static final String COL_ORDER_STATUS = "order_status";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}