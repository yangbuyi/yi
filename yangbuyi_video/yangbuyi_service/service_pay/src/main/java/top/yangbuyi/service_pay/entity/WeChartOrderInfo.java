// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <pre>
 *  订单响应
 * </pre>
 *
 * @author 杨不易呀
 * @verison $Id: WeChartTOrderInfo v 0.1 2022-11-16 00:43:40
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class WeChartOrderInfo {

    /**
     * <pre>
     * amount
     * </pre>
     */
    private weChartOrderAmount amount;

    /**
     * <pre>
     *
     * </pre>
     */
    private String appid;

    /**
     * <pre>
     *
     * </pre>
     */
    private String attach;

    /**
     * <pre>
     *
     * </pre>
     */
    private String bank_type;

    /**
     * <pre>
     *
     * </pre>
     */
    private String mchid;

    /**
     * <pre>
     *
     * </pre>
     */
    private String out_trade_no;

    /**
     * <pre>
     * payer
     * </pre>
     */
    private weChartPayer payer;

    /**
     * <pre>
     * promotion_detail
     * </pre>
     */
    private List<String> promotion_detail;

    /**
     * <pre>
     *
     * </pre>
     */
    private String success_time;

    /**
     * <pre>
     *
     * </pre>
     */
    private String trade_state;

    /**
     * <pre>
     * 支付成功
     * </pre>
     */
    private String trade_state_desc;

    /**
     * <pre>
     *
     * </pre>
     */
    private String trade_type;

    /**
     * <pre>
     *
     * </pre>
     */
    private String transaction_id;


}
