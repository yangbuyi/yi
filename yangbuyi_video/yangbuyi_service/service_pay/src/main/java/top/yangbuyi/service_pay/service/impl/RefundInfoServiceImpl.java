// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top.yangbuyi.common_utils.enums.weChatPay.WxRefundStatus;
import top.yangbuyi.common_utils.utils.GsonUtils;
import top.yangbuyi.service_pay.entity.PayOrder;
import top.yangbuyi.service_pay.entity.RefundInfo;
import top.yangbuyi.service_pay.mapper.RefundInfoMapper;
import top.yangbuyi.service_pay.service.PayOrderService;
import top.yangbuyi.service_pay.service.RefundInfoService;
import top.yangbuyi.service_pay.utils.OrderNoUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class RefundInfoServiceImpl extends ServiceImpl<RefundInfoMapper, RefundInfo> implements RefundInfoService {

    private final PayOrderService payOrderService;


    @Override
    public void updateRefund(String content) {
        //将json字符串转换成Map
        Map<String, String> resultMap = GsonUtils.toObject(content, HashMap.class);

        //根据退款单编号修改退款单
        QueryWrapper<RefundInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("refund_no", resultMap.get("out_refund_no"));

        //设置要修改的字段
        RefundInfo refundInfo = new RefundInfo();

        refundInfo.setRefundId(resultMap.get("refund_id"));//微信支付退款单号

        //查询退款和申请退款中的返回参数
        if (resultMap.get("status") != null) {
            refundInfo.setRefundStatus(resultMap.get("status"));//退款状态
            refundInfo.setContentReturn(content);//将全部响应结果存入数据库的content字段
        }

        //退款回调中的回调参数
        if (resultMap.get("refund_status") != null) {
            refundInfo.setRefundStatus(resultMap.get("refund_status"));//退款状态
            refundInfo.setContentNotify(content);//将全部响应结果存入数据库的content字段
        }

        //更新退款单
        baseMapper.update(refundInfo, queryWrapper);
    }

    @Override
    public RefundInfo createRefundByOrderNo(String orderNo, String reason) {
        //根据订单号获取订单信息
        PayOrder orderInfo = this.payOrderService.lambdaQuery().eq(PayOrder::getOrderNo, orderNo).one();

        //根据订单号生成退款订单
        RefundInfo refundInfo = new RefundInfo();
        refundInfo.setOrderNo(orderNo);//订单编号
        refundInfo.setRefundNo(OrderNoUtils.getRefundNo());//退款单编号
        int totalFee = OrderNoUtils.getTotalFee(orderInfo.getTotalFee().doubleValue());
        refundInfo.setTotalFee(totalFee);//原订单金额(分)
        refundInfo.setRefund(totalFee);//退款金额(分)
        refundInfo.setReason(reason);//退款原因

        //保存退款订单
        baseMapper.insert(refundInfo);

        return refundInfo;
    }

    @Override
    public List<RefundInfo> getNoRefundOrderByDuration(int minutes) {
        // minutes 分钟之前的时间 查询 当前时间减去 minutes时间 = 要查询的时间之内的退款订单
        Instant instant = Instant.now().minus(Duration.ofMinutes(minutes));

        return this.lambdaQuery().eq(RefundInfo::getRefundStatus, WxRefundStatus.PROCESSING.getType())
                .le(RefundInfo::getGmtCreate, instant).list();
    }
}
