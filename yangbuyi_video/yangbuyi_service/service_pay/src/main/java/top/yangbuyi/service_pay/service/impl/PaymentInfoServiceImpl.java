// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_pay.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.yangbuyi.common_utils.enums.weChatPay.PayType;
import top.yangbuyi.common_utils.utils.GsonUtils;
import top.yangbuyi.service_pay.entity.PaymentInfo;
import top.yangbuyi.service_pay.mapper.PaymentInfoMapper;
import top.yangbuyi.service_pay.service.PaymentInfoService;

import java.math.BigDecimal;
import java.util.Map;

@Service
@Slf4j
public class PaymentInfoServiceImpl extends ServiceImpl<PaymentInfoMapper, PaymentInfo> implements PaymentInfoService {

    @Override
    public void createPaymentInfo(String plainText) {
        log.info("记录支付日志");
        Map<String, Object> plainTextMap = GsonUtils.toObject(plainText, Map.class);

        //订单号
        String orderNo = (String) plainTextMap.get("out_trade_no");
        //业务编号
        String transactionId = (String) plainTextMap.get("transaction_id");
        //支付类型
        String tradeType = (String) plainTextMap.get("trade_type");
        //交易状态
        String tradeState = (String) plainTextMap.get("trade_state");
        //用户实际支付金额
        Map<String, Object> amount = (Map<String, Object>) plainTextMap.get("amount");

        // 此字段需要一个合法的 64 位有符号整数
        int payerTotal = BigDecimal.valueOf((Double) amount.get("payer_total")).multiply(BigDecimal.valueOf(100)).intValue();
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setOrderNo(orderNo);
        paymentInfo.setPaymentType(PayType.WXPAY.getType());
        paymentInfo.setTransactionId(transactionId);
        paymentInfo.setTradeType(tradeType);
        paymentInfo.setTradeState(tradeState);
        paymentInfo.setPayerTotal(payerTotal);
        paymentInfo.setContent(plainText);

        baseMapper.insert(paymentInfo);
    }

    @Override
    public void createPaymentInfoV2(Map<String, String> plainTextMap) {
        log.info("记录V2支付日志");

        //订单号
        String orderNo = plainTextMap.get("out_trade_no");
        //业务编号
        String transactionId = plainTextMap.get("transaction_id");
        //支付类型
        String tradeType = plainTextMap.get("trade_type");
        //交易状态
        String tradeState = plainTextMap.get("return_code");
        //用户实际支付金额
        Integer payerTotal = Integer.valueOf(plainTextMap.get("total_fee"));

        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setOrderNo(orderNo);
        paymentInfo.setPaymentType(PayType.WXPAY.getType());
        paymentInfo.setTransactionId(transactionId);
        paymentInfo.setTradeType(tradeType);
        paymentInfo.setTradeState(tradeState);
        paymentInfo.setPayerTotal(payerTotal);
        paymentInfo.setContent(new Gson().toJson(plainTextMap));

        baseMapper.insert(paymentInfo);
    }
}
