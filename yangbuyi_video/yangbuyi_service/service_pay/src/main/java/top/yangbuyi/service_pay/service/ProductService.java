// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.service_pay.entity.delete.Product;

public interface ProductService extends IService<Product> {


}
