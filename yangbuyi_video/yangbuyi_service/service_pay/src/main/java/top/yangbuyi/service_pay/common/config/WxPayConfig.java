// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.common.config;

import com.wechat.pay.contrib.apache.httpclient.WechatPayHttpClientBuilder;
import com.wechat.pay.contrib.apache.httpclient.auth.PrivateKeySigner;
import com.wechat.pay.contrib.apache.httpclient.auth.ScheduledUpdateCertificatesVerifier;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Credentials;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Validator;
import com.wechat.pay.contrib.apache.httpclient.util.PemUtil;

import lombok.Data;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import top.yangbuyi.common_utils.exception.BusinessException;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;

@Configuration
@PropertySource("classpath:wxpay.properties") //读取配置文件
@ConfigurationProperties(prefix = "wxpay") //读取wxpay节点
@Data
public class WxPayConfig {
    private static final Logger log = LoggerFactory.getLogger("WxPayConfig");


    // 商户号
    private String mchId;

    // 商户API证书序列号
    private String mchSerialNo;

    // 商户私钥文件
    private String privateKeyPath;

    // APIv3密钥
    private String apiV3Key;

    // APPID
    private String appid;

    // 微信服务器地址
    private String domain;

    // 接收结果通知地址
    private String notifyDomain;

    // APIv2密钥
    private String partnerKey;


    /**
     * 获取商户的私钥文件
     */
    public PrivateKey getPrivateKey(String filename) {
//        原版
//        try {
//            return PemUtil.loadPrivateKey(new FileInputStream(filename));
//        } catch (FileNotFoundException e) {
//            throw new BusinessException("私钥文件不存在", e);
//        }

        try {
            InputStream inputStream = Bean.class.getClassLoader().getResourceAsStream(filename);
            assert inputStream != null;
            return PemUtil.loadPrivateKey(inputStream);
        } catch (Exception e) {
            throw new BusinessException("私钥文件不存在", e);
        }
    }

    /**
     * 获取签名验证器
     */
    @Bean
    public ScheduledUpdateCertificatesVerifier getVerifier() {

        log.info("构建签名验证器");
        log.info("mchId: {}", mchId);
        log.info("mchSerialNo: {}", mchSerialNo);
        log.info("privateKeyPath: {}", privateKeyPath);
        log.info("apiV3Key: {} , {}", apiV3Key, apiV3Key.length());
        log.info("appid: {}", appid);
        log.info("domain: {}", domain);
        log.info("notifyDomain: {}", notifyDomain);
        log.info("partnerKey: {} , {}", partnerKey, partnerKey.length());

        //获取商户私钥
        PrivateKey privateKey = getPrivateKey(privateKeyPath);

        //私钥签名对象
        PrivateKeySigner privateKeySigner = new PrivateKeySigner(mchSerialNo, privateKey);

        //身份认证对象
        WechatPay2Credentials wechatPay2Credentials = new WechatPay2Credentials(mchId, privateKeySigner);

        // 使用定时更新的签名验证器，不需要传入证书 -> 它会帮助我们下载最新的证书不会进行过期
        return new ScheduledUpdateCertificatesVerifier(wechatPay2Credentials, apiV3Key.getBytes(StandardCharsets.UTF_8));
    }


    /**
     * 获取http请求对象
     */
    @Lazy
    @Bean("wxPayClient")
    public CloseableHttpClient wxPayClient(ScheduledUpdateCertificatesVerifier verifier) {

        log.info("获取httpClient");

        //获取商户私钥
        PrivateKey privateKey = getPrivateKey(privateKeyPath);

        WechatPayHttpClientBuilder builder = WechatPayHttpClientBuilder.create()
                // 设置商户信息
                .withMerchant(mchId, mchSerialNo, privateKey)
                // 设置验签器
                .withValidator(new WechatPay2Validator(verifier));
        // ... 接下来，你仍然可以通过builder设置各种参数，来配置你的HttpClient

        // 通过WechatPayHttpClientBuilder构造的HttpClient，会自动的处理签名和验签，并进行证书自动更新
        return builder.build();
    }

    /**
     * 获取HttpClient，无需进行应答签名验证，跳过验签的流程
     */
//    @Lazy
//    @Bean("wxPayNoSignClient")
//    public CloseableHttpClient wxPayNoSignClient() {
//
//        //获取商户私钥
//        PrivateKey privateKey = getPrivateKey(privateKeyPath);
//
//        //用于构造HttpClient
//        WechatPayHttpClientBuilder builder = WechatPayHttpClientBuilder.create()
//                //设置商户信息
//                .withMerchant(mchId, mchSerialNo, privateKey)
//                //无需进行签名验证、通过withValidator((response) -> true)实现
//                .withValidator((response) -> true);
//
//        // 通过WechatPayHttpClientBuilder构造的HttpClient，会自动的处理签名和验签，并进行证书自动更新
//        CloseableHttpClient httpClient = builder.build();
//
//        log.info("== getWxPayNoSignClient END ==");
//
//        return httpClient;
//    }

}
