// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.service;


import top.yangbuyi.service_pay.entity.TradeBillResponse;
import top.yangbuyi.service_pay.entity.WeChartOrderInfo;

import java.security.GeneralSecurityException;
import java.util.Map;

/**
 * @author yang shuai
 * @date 2022/11/13
 */

public interface WxPayService {
    /**
     * 统一调用下单API，生成支付二维码
     * @param contentId 商户ID
     */
    Map<String, Object> nativePay(Long contentId) throws Exception;

    void processOrder(Map<String, Object> bodyMap) throws GeneralSecurityException, InterruptedException;

    void cancelOrder(String orderNo) throws Exception;

    WeChartOrderInfo queryOrder(String orderNo) throws Exception;

    void checkOrderStatus(String orderNo) throws Exception;

    void refund(String orderNo, String reason, String refundsNo) throws Exception;

    String queryRefund(String refundNo) throws Exception;

    void checkRefundStatus(String refundNo) throws Exception;

    void processRefund(Map<String, Object> bodyMap) throws Exception;

    TradeBillResponse queryBill(String billDate, String type) throws Exception;

    String downloadBill(String billDate, String type) throws Exception;

    Map<String, Object> nativePayV2(Long productId, String remoteAddr) throws Exception;
}

