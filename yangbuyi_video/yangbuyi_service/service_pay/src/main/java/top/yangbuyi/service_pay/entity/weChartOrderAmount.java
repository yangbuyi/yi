// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author yang shuai
 * @date 2022/11/16
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class weChartOrderAmount {


    /**
     * <pre>
     *
     * </pre>
     */
    private String currency;

    /**
     * <pre>
     *
     * </pre>
     */
    private String payer_currency;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer payer_total;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer total;


}
