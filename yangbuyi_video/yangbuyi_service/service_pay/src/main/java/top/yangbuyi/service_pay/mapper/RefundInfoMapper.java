// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.service_pay.entity.RefundInfo;

public interface RefundInfoMapper extends BaseMapper<RefundInfo> {
}