// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.service_pay.entity.delete.OrderInfo;

import java.util.List;

public interface OrderInfoService extends IService<OrderInfo> {


    /**
     * 创建订单
     *
     * @param productId 商品
     */
    OrderInfo createOrderByProductId(Long productId);

    /**
     * 查询超过五分钟的未支付的订单
     */
    List<OrderInfo> getNoPayOrderByDuration(int minutes);
}

