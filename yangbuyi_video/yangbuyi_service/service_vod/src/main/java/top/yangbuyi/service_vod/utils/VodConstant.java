// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.service_vod.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @program: yangbuyi_video
 * @ClassName: VodConstant
 * @create: 2021-09-23 02:40
 * @author: Yang Shuai
 * @since： JDK1.8
 * @VodConstant: 常量$
 **/

@Component
@RefreshScope
public class VodConstant implements InitializingBean {

    /**
     * 地区
     */
    @Value("${aliyun.regionId}")
    private String regionId;

    /**
     * 访问密钥id
     */
    @Value("${aliyun.accessKey}")
    private String accessKeyId;

    /**
     * 访问密钥的秘密
     */
    @Value("${aliyun.accessSecret}")
    private String accessKeySecret;


    /**
     * 地区
     */
    public static String REGION_ID;

    /**
     * 评估关键id
     */
    public static String ASSESS_KEY_ID;
    /**
     * 评估关键秘密
     */
    public static String ASSESS_KEY_SECRET;

    /**
     * 在属性文件加载完毕后属性也设置完毕之后, 会自动调用
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet () throws Exception {
        REGION_ID = regionId;
        ASSESS_KEY_ID = accessKeyId;
        ASSESS_KEY_SECRET = accessKeySecret;
    }
}