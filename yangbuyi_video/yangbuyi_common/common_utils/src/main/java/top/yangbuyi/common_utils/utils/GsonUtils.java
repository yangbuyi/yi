// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.common_utils.utils;

import com.google.gson.Gson;

/**
 * 谷歌 序列化
 * @author yang shuai
 * @date 2022/12/22
 */
public class GsonUtils {

    static Gson gson = new Gson();

    public static String toJsonStr(Object obj) {
        return gson.toJson(obj);
    }

    public static <T> T toObject(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }

}
