// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.common_utils.enums.weChatPayJSAPI;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * JSAPI 接口枚举
 *
 * @author yang shuai
 * @date 2022/11/19
 */
@AllArgsConstructor
@Getter
public enum WxJSApiType {

    /**
     * jsapi 下单
     * POST
     */
    JSAPI_PAy("/v3/pay/transactions/jsapi");


    /**
     * 类型
     */
    private final String type;
}
