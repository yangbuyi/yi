// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.common_utils.enums.wxpay;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单状态
 */
@AllArgsConstructor
@Getter
public enum OrderStatus {
    /**
     * 未支付
     */
    NOTPAY(0,"未支付"),


    /**
     * 支付成功
     */
    SUCCESS(1,"支付成功"),

    /**
     * 已关闭
     */
    CLOSED(2,"超时已关闭"),

    /**
     * 已取消
     */
    CANCEL(3,"用户已取消"),

    /**
     * 退款中
     */
    REFUND_PROCESSING(4,"退款中"),

    /**
     * 已退款
     */
    REFUND_SUCCESS(5,"已退款"),

    /**
     * 退款异常
     */
    REFUND_ABNORMAL(6,"退款异常");

    /**
     * 类型
     */
    private final int code;
    private final String type;

}
