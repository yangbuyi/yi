// Yang Shuai  Copyright (c) 2022 https://yby6.com.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you
//

package top.yangbuyi.common_utils.enums.weChatPayNative;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Native回调枚举
 * 商户服务接收的回调 API 接口
 */
@AllArgsConstructor
@Getter
public enum WxNotifyType {

    /**
     * 支付通知 v3
     * /v1/play/callback
     * /api/wx-pay/native/notify
     */
    NATIVE_NOTIFY("/service_pay/wx-pay/native/notify"),

    /**
     * 支付通知 v2
     */
    NATIVE_NOTIFY_V2("/service_pay/wx-pay-v2/native/notify"),

    /**
     * 退款结果通知
     */
    REFUND_NOTIFY("/service_pay/wx-pay/refunds/notify");

    /**
     * 类型
     */
    private final String type;
}
